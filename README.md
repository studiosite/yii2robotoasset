
yii2robotoasset
=================

Бандл шрифта [Google Roboto](https://www.google.com/fonts/specimen/Roboto)

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Необходимо добавить

```json
"studiosite/yii2robotoasset": "*"
```

в секции ```require``` `composer.json` файла.

В секции ```repositories``` добавить

```json
{
    "type": "vcs",
    "url": "https://bitbucket.org/studiosite/yii2robotoasset"
}
```

### Использование

В отображении

```php
use studiosite\yii2robotoasset\RobotoAsset;

RobotoAsset::register($this);
```

