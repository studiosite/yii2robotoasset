<?php

namespace studiosite\yii2robotoasset;

use Yii;
use yii\web\AssetBundle;

/**
 * Шрифт roboto
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $baseUrl
 * @property string $sourcePath
 * @property array $css
 * @property array $js
 * @property array $depends
 */
class RobotoAsset extends AssetBundle
{
	/**
    * @var string Альяс пути где находятся асетсы
    */
	public $baseUrl = '@web';

	/**
    * @var array Исходный путь
    */
    public $sourcePath = '@studiosite/yii2robotoasset/assets';

	/**
	* Инициализация
	*/
	public function init()
	{
		Yii::setAlias('@studiosite/yii2robotoasset', __DIR__);
	}

	/**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
		'https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,500,500italic,300,300italic',
        'css/roboto.scss'
    ];

	/**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
    ];
}
